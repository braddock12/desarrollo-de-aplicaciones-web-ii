<?php
// Inicializar variables
$numeros = array();
$repetidos = 0;

// Verificar si se ha enviado el formulario
if (isset($_POST["btnCalcular"])) {
    // Obtener los números ingresados por el usuario
    for ($i = 1; $i <= 6; $i++) {
        $numeros[] = (int)$_POST["txtNumero$i"];
    }

    // Contar los números repetidos
    foreach (array_count_values($numeros) as $numero => $cantidad) {
        if ($cantidad > 1) {
            $repetidos++;
        }
    }
}
?>

<html>
<head>
    <title>Determinar números repetidos</title>
    <style type="text/css">
        body {
            background-color: #ff0000;
            color: #000000;
        }
        input[type="text"], input[type="submit"] {
            background-color: #000000;
            color: #ffffff;
        }
    </style>
</head>

<body>
    <link rel="stylesheet" href="estilo03.css">
    <form method="post" action="">
        <table width="242" border="0">
            <tr>
                <td colspan="2"><strong>Determinar números repetidos</strong> </td>
            </tr>
            <tr>
                <td>Ingrese 6 números: </td>
                <td>
                    <?php for ($i = 1; $i <= 6; $i++): ?>
                        <input name="txtNumero<?= $i ?>" type="text" id="txtNumero<?= $i ?>" value="<?= isset($numeros[$i-1]) ? $numeros[$i-1] : '' ?>" />
                    <?php endfor; ?>
                </td>
            </tr>
            <tr>
                <td>Números repetidos:</td>
                <td>
                    <input name="txts" type="text" class="TextoFondo" id="txts" value="<?= $repetidos ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
