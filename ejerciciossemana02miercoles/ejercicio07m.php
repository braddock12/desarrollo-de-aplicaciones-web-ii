
<!DOCTYPE html>
<html>
<head>
	<title>Frase con asteriscos</title>
	<style>
		body {
			font-family: Arial, sans-serif;
			font-size: 18px;
		}
	</style>
</head>
<body>
    <link rel="stylesheet" href="estilo07.css">
	<?php
	$frase = "soy marco y estudio programacion";
	$frase_con_asteriscos = str_replace(' ', '*', $frase);
	?>
	<p><strong>Frase original:</strong> <?php echo $frase; ?></p>
	<p><strong>Frase con asteriscos:</strong> <?php echo $frase_con_asteriscos; ?></p>
</body>
</html>
