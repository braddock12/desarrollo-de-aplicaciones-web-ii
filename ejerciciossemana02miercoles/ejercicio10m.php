<!DOCTYPE html>
<html>
<head>
	<title>Encriptación de Frase</title>
	
</head>
<body>
    <link rel="stylesheet" href="estilo10.css">
	<h1>Encriptación de Frase</h1>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="frase">Ingrese la frase:</label><br>
		<textarea id="frase" name="frase" rows="5" cols="40"><?php if(isset($_POST['frase'])) echo $_POST['frase']; ?></textarea><br>
		<input type="submit" value="Encriptar">
	</form>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$frase = $_POST['frase'];
			$frase_encriptada = "";

			for ($i=0; $i<strlen($frase); $i++) {
				$caracter_ascii = ord($frase[$i]);
				$caracter_encriptado_ascii = $caracter_ascii + 2;
				$caracter_encriptado = chr($caracter_encriptado_ascii);
				$frase_encriptada .= $caracter_encriptado;
			}

			echo "<p>La frase encriptada es:</p>";
			echo "<p>$frase_encriptada</p>";
		}
	?>
</body>
</html>
