<?php
// Inicializar la matriz
$matriz = array(
    array(0, 0),
    array(0, 0),
    array(0, 0)
);

// Verificar si se ha enviado el formulario
if (isset($_POST["btnCalcular"])) {
    // Leer los valores de la matriz
    $matriz[0][0] = (int)$_POST["txtn1"];
    $matriz[0][1] = (int)$_POST["txtn2"];
    $matriz[1][0] = (int)$_POST["txtn3"];
    $matriz[1][1] = (int)$_POST["txtn4"];
    $matriz[2][0] = (int)$_POST["txtn5"];
    $matriz[2][1] = (int)$_POST["txtn6"];

    // Calcular el promedio
    $sum = 0;
    for ($i = 0; $i < 3; $i++) {
        for ($j = 0; $j < 2; $j++) {
            $sum += $matriz[$i][$j];
        }
    }
    $promedio = $sum / 6;
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Promedio de una matriz</title>
    
</head>
<body>
    <link rel="stylesheet" href="estilo04.css">
    <div id="contenedor">
        <h1>Promedio de una matriz</h1>
        <form method="post" action="">
            <label>Ingrese los valores de la matriz:</label>
            <table>
                <tr>
                    <td><input name="txtn1" type="text" value="<?= $matriz[0][0] ?>"></td>
                    <td><input name="txtn2" type="text" value="<?= $matriz[0][1] ?>"></td>
                </tr>
                <tr>
                    <td><input name="txtn3" type="text" value="<?= $matriz[1][0] ?>"></td>
                    <td><input name="txtn4" type="text" value="<?= $matriz[1][1] ?>"></td>
                </tr>
                <tr>
                    <td><input name="txtn5" type="text" value="<?= $matriz[2][0] ?>"></td>
                    <td><input name="txtn6" type="text" value="<?= $matriz[2][1] ?>"></td>
                </tr>
            </table>
            <input type="submit" name="btnCalcular" value="Calcular promedio">
        </form>
        <?php if (isset($_POST["btnCalcular"])): ?>
            <div class="resultado">
                El promedio es: <?= $promedio ?>
            </div>
        <?php endif; ?>
    </div>
</body>
</html>
